<?php

/**
 * Author: Evandro Ishy Medeiros
 * Description: Classe criada para geração de base de dados e tabelas.
 */

Class GenerateDb Extends AbstractGeneric
{
    protected $path;
    protected $name;
    protected $error;

    /**
     * Construtor que pode receber os dados de um segundo banco utilizado para testes
     */
    public function __construct($dbPath=DB_PATH, $dbName=DB_NAME)
    {
        $this->path = $dbPath;
        $this->name = $dbName;
        $this->error = "";
    }

    /**
     * Cria o banco de dados caso nao exista e retorna True caso seja necessario criar as tabelas
     */
    public function createDb()
    {
        if(file_exists($this->path.$this->name))
        {
            $this->error = "O arquivo de banco já existe.\n";
            return False;
        }
        /*Funcao herdada da classe abstrata*/
        $db = $this->newSqliteConnection();
        $db->close();
        
        return True;
    }

    /**
     * Cria as tabelas necessarias para armazenamento.
     */
    public function createTables()
    {
        /*Funcao herdada da classe abstrata*/
        $db = $this->newSqliteConnection();
        
        try
        {
            $db->exec("CREATE TABLE deputados (id integer, nome varchar(255))");
            $db->exec("CREATE TABLE indenizacoes (deputado_id integer, despesa_id integer, valor real)");
            $db->exec("CREATE TABLE despesas(id integer, nome varchar(255))");
        }
        catch(Exception $e)
        {
            $this->error = "Ocorreu um erro durante a criacao das tabelas.\n";
            return False;
        }

        $db->close();
        return True;
    }
}