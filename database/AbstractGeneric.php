<?php

/**
 * Author: Evandro Ishy Medeiros
 * Description: Classe abstrata genérica para retorno de erro pelos models e pelas classes utilizadas
 * para criação e alimentação do banco de dados
 */

Abstract Class AbstractGeneric
{
    /**
     * Método de retorno de erros
     */
    public function getError()
    {
        return $this->error;
    }

    /** 
     * Ponto unificado para criação de conexão com o sqlite
     */
    protected function newSqliteConnection()
    {
        return new Sqlite3($this->path.$this->name);
    }

    /**
     * Método para transformar os rows do sqlite em um array
     */
    protected function getArray($source)
    {
        $retorno = [];
        while($row = $source->fetchArray())
            $retorno[] = $row;

        return $retorno;
    }
}