<?php

/**
 * Author: Evandro Ishy Medeiros
 * Description: Classe de alimentação do banco de dados.
 * A mesma conecta no ws utilizando uma função get, encontra-se no src/Helper.php,
 * E faz as chamdas dos devidos models para tratamento de cada um dos elementos a serem inseridos.
 */

Class InsertInfosInDb Extends AbstractGeneric
{

    protected $path;
    protected $name;
    protected $error;

    public function __construct($dbPath=DB_PATH, $dbName=DB_NAME)
    {
        $this->path = $dbPath;
        $this->name = $dbName;
        $this->error = "";
    }

    /**
     * Método para conexão no ws para consulta dos deputados em exercício
     */
    public function getDeputados()
    {
        /*Necessario um objeto da classe deputado para insert*/
        $objDeputado = new Deputado();

        /*Caso a tabela já possua dados é considerado que não há motivo para reinserção*/
        if(!empty($objDeputado->get()))
        {
            $this->error = "A tabela deputado ja foi alimentada\n";
            return False;
        }
        /*Busca os dados no ws*/
        $resultado = get(URL.'ws/deputados/em_exercicio?formato=json');
  
        /*Valida se o retorno é um json, em caso de erro não será um json*/
        if(isJson($resultado))
        {          
            $arrayDeputados = json_decode($resultado);

            if(empty($arrayDeputados))
            {
                $this->error = "Dificuldade de conexao";
                return False;
            }
            
            $arrayDeputados = $arrayDeputados->list;

            foreach($arrayDeputados as $deputado)
                $objDeputado->insert($deputado);

            return True;
        }

        return False;
    }

    /**
     * Método que busca as indenizações e como o mesmo retorna o nome da despesa e o id também faz a inserção
     * de despesa e id na tabela despesas
     */
    public function getIndenizacoes()
    {
        $objDeputado = new Deputado();
        $objDespesa = new Despesa();
        $objIndenizacao = new Indenizacao();

        /*Busca todos os ids dos deputados previamente armazenados no banco de dados*/
        $listaDeputados = $objDeputado->get(['id']);

        /*Percorre cada deputado verificando as despesas do mesmo em cada mês do ano de 2014*/
        foreach($listaDeputados as $deputado)
        {
            /*"Gera" url para cada deputado*/
            $url = URL . 'ws/prestacao_contas/verbas_indenizatorias/deputados/'. $deputado['id']. '/2014/{mes}?formato=json';
            for($i=1;$i <=12; $i++)
            {
                /*Altera apenas o mês para fazer a requisição*/
                $resultado = get(str_replace("{mes}", $i, $url));

                /*Verifica se é um json valido*/
                if(isJson($resultado))
                {
                    $umDepInd = json_decode($resultado);
                    foreach($umDepInd->list as $umaIndenizacao)
                    {
                        /*Organiza os dados para inserção*/
                        $data = new stdClass();
                        $data->despesa_id = $umaIndenizacao->codTipoDespesa;
                        $data->despesa_nome = $umaIndenizacao->descTipoDespesa;

                        /*Inicialmente é feita a inserção da despesa*/
                        if($objDespesa->insert($data))
                            echo $objDespesa->message;

                        $data->valor = $umaIndenizacao->valor;
                        $data->deputado_id = $umaIndenizacao->idDeputado;

                        /*Depois é feita a inserção ou update da indenização(Explicação no model de indenização)*/
                        if($objIndenizacao->insert($data))
                            echo $objIndenizacao->message;
                    }
                    /*Medida para tornar explicita que o resultado é zerado a cada iteração*/
                    $reultado = '';
                }
            }
        }
    }
}
