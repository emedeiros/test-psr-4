<?php

include(__DIR__.'/constants.php');

include(__DIR__.'/../src/Helper.php');

include(__DIR__.'/../database/AbstractGeneric.php');
include(__DIR__.'/../database/GenerateDb.php');
include(__DIR__.'/../database/InsertInfosInDb.php');

include(__DIR__.'/../src/Models/Deputado.php');
include(__DIR__.'/../src/Models/Indenizacao.php');
include(__DIR__.'/../src/Models/Despesa.php');

include(__DIR__.'/../src/Controller.php');
