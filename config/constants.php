<?php
/*Constantes do banco*/
define('DB_NAME', 'database.sqlite');
define('DB_NAME_TEST', 'database_test.sqlite');
define('DB_PATH', __DIR__.'/../storage/');

/*Limite de pesquisa pre definido para todos*/
define('LIMIT_SEARCH', 5);

/*URL do webservice*/
define('URL', 'http://dadosabertos.almg.gov.br/');