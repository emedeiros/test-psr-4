<?php

include(__DIR__."/config/autoload.php");

$controller = new Controller();

if(!isset($_GET['categoria']))
    $data = $controller->categorias();
else
    $data = $controller->deputados($_GET['categoria']);

?><!DOCTYPE html>
<html lang="pt_BR">
    <head>
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <title><?php echo $data['titulo']; ?></title>
        <meta name="author" content="Evandro Ishy Medeiros">
        <style type="text/css">
              .click{cursor: pointer; color: #1a0dab;}
        </style>
    </head>
    <body>
        <div class="container main">
            <div class="row">
                <div class="col-md-2">
                    <a class="link" href="index.php"><h1>Inicio</h1></a>
                </div>
                <div class="col-md-10">
                    <h1><?php echo $data['page_h1']; ?></h1>
                </div>
            </div>
            <hr/>
            <?php if($data['message'] !== "") {?>
                <div class="row">
                    <div class="col-md-offset-3 col-md-6">
                        <div class="alert alert-danger" role="alert">
                            <span class="sr-only">Error:</span><?php echo $data['message']; ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <div class="row">
                <table class="table table-striped">
                    <tr>
                        <th><?php echo $data['colName1']; ?></th>
                        <th><?php echo $data['colName2']; ?></th>
                    </tr>
                    <?php foreach($data['tabela'] as $tabela) {?>
                        <tr class="<?php echo $data['class_tr']; ?>" data-id="<?php echo $tabela['id']; ?>">
                            <td class="col-md-9"><?php echo $tabela['nome'];  ?></td>
                            <td class="col-md-3">R$ <?php echo str_replace(".", ",",money_format("%i",$tabela['valor'])); ?></td>
                        </tr>
                    <?php }?>
                </table>
            </div>
        </div>
    </body>
    <script src="assets/js/jquery-1.11.3.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/script.js"></script>
</html>