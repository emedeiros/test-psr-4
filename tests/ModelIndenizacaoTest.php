<?php

Class ModelIndenizacaoTest Extends PHPUnit_Framework_TestCase
{

    public function __construct()
    {
        $this->testClass = new Indenizacao(DB_PATH, DB_NAME_TEST);
    }

    public function __destruct()
    {
        if(file_exists(DB_PATH.DB_NAME_TEST))
            unlink(DB_PATH.DB_NAME_TEST);
    }

    public function testInsertShouldReturnFalseWhenSomeIdIsInvalid()
    {

        $dbClass = new GenerateDb(DB_PATH, DB_NAME_TEST);
        if(!file_exists(DB_PATH.DB_NAME_TEST))
        {   
            $dbClass->createDb();
            $dbClass->createTables();
        }

        $data = new stdClass();
        $data->despesa_id = 0;
        $data->deputado_id = "";
        $data->valor = "100.00";
        $this->assertEquals(False, $this->testClass->insert($data));
        $this->assertEquals("Dado inválido\n", $this->testClass->getError());
    }

    public function testInsertShouldReturnTrueWhenNameAndIdIsOk()
    {
        $data = new stdClass();
        $data->despesa_id = 35;
        $data->deputado_id = 35;
        $data->valor = "100.00";
        $this->assertEquals(True, $this->testClass->insert($data));        
    }

    public function testGetShouldReturnEmptyArray()
    {
        $data = new stdClass();
        $data->despesa_id = 0;
        $data->deputado_id = 0;
        $this->assertEquals(True, empty($this->testClass->get($data)));
    }

    public function testGetShouldReturnArrayWithData()
    {
        $data = new stdClass();
        $data->despesa_id = 35;
        $data->deputado_id = 35;
        $this->assertEquals(False, empty($this->testClass->get($data)));   
    }
}
