<?php

Class CreateDbTest Extends PHPUnit_Framework_TestCase
{

    private $testClass;
    private $file;

    public function __construct()
    {
        $this->testClass = new GenerateDb(DB_PATH, DB_NAME_TEST);
        $this->file = DB_PATH.DB_NAME_TEST;
    }

    public function testCreateDShouldReturnTrueWhenDbFileNotExist()
    {
        if(file_exists($this->file))
            unlink($this->file);

        $this->assertEquals(True, $this->testClass->createDb());
    }

    public function testCreateDbShouldReturnFalseWhenDbFileExist()
    {
        if(!file_exists($this->file))
            file_put_contents($this->file, '');

        $this->assertEquals(False, $this->testClass->createDb());
        $this->assertEquals("O arquivo de banco já existe.\n", $this->testClass->getError());

        unlink($this->file);
    }

    /*Implementar validação extra para verificar se  o dono da pasta esta correto*/

    public function testCreateTablesShouldReturnTrueWhenTableNotExist()
    {
        $this->testClass->createDb();
        $this->assertEquals(True, $this->testClass->createTables());

        unlink($this->file);
    }
}
