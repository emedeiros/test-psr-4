<?php

Class ModelDespesaTest Extends PHPUnit_Framework_TestCase
{

    public function __construct()
    {
        $this->testClass = new Despesa(DB_PATH, DB_NAME_TEST);
    }

    public function __destruct()
    {
        if(file_exists(DB_PATH.DB_NAME_TEST))
            unlink(DB_PATH.DB_NAME_TEST);
    }

    public function testInsertShouldReturnFalseWhenNameOrIdIsBlank()
    {

        $dbClass = new GenerateDb(DB_PATH, DB_NAME_TEST);
        if(!file_exists(DB_PATH.DB_NAME_TEST))
        {   
            $dbClass->createDb();
            $dbClass->createTables();
        }

        $data = new stdClass();
        $data->despesa_id = 0;
        $data->despesa_nome = "";
        $this->assertEquals(False, $this->testClass->insert($data));
        $this->assertEquals("Dado inválido\n", $this->testClass->getError());
    }

    public function testInsertShouldReturnTrueWhenNameAndIdIsOk()
    {
        $data = new stdClass();
        $data->despesa_id = 35;
        $data->despesa_nome = "Teste";
        $this->assertEquals(True, $this->testClass->insert($data));        
    }

    public function testGetShouldReturnEmptyArray()
    {
        $this->assertEquals(True, empty($this->testClass->get(-10)));
    }

    public function testGetShouldReturnArrayWithData()
    {
        $this->assertEquals(False, empty($this->testClass->get(35)));   
    }
}
