<?php

Class ModelDeputadoTest Extends PHPUnit_Framework_TestCase
{
    private $testClass;

    public function __construct()
    {
        $this->testClass = new Deputado(DB_PATH, DB_NAME_TEST);
    }

    public function __destruct()
    {
        if(file_exists(DB_PATH.DB_NAME_TEST))
            unlink(DB_PATH.DB_NAME_TEST);
    }

    public function testInsertShouldReturnFalseWhenNameOrIdIsBlank()
    {

        $dbClass = new GenerateDb(DB_PATH, DB_NAME_TEST);
        if(!file_exists(DB_PATH.DB_NAME_TEST))
        {   
            $dbClass->createDb();
            $dbClass->createTables();
        }

        $data = new stdClass();
        $data->id = 0;
        $data->nome = "";
        $this->assertEquals(False, $this->testClass->insert($data));
        $this->assertEquals("Dado Inválido\n", $this->testClass->getError());
    }

    public function testInsertShouldReturnTrueWhenNameAndIdIsOk()
    {
        $data = new stdClass();
        $data->id = 14121989;
        $data->nome = "Evandro 'Ishy";
        $this->assertEquals(True, $this->testClass->insert($data));        
    }

    public function testGetShouldReturnEmptyArray()
    {
        $this->assertEquals(True, empty($this->testClass->get(['id'], 1)));
    }

    public function testGetShouldReturnArrayWithData()
    {
        $this->assertEquals(False, empty($this->testClass->get(['id'])));
    }

}
