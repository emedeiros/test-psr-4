<?php

Class HelperTest Extends PHPUnit_Framework_TestCase
{
    public function testGetShouldReturnStringOfEmptyArray()
    {
        $this->assertEquals("[]", get("htasd"));
    }

    public function testGetShouldReturnStringWithData()
    {
        $this->assertGreaterThan(2, strlen(get(URL)));
    }

    public function testIsJsonShouldReturnFalse()
    {
        $this->assertEquals(False, isJson("This not a json string"));
    }

    public function testIsJsonShouldReturnTrue()
    {
        $this->assertEquals(True, isJson("[]"));
    }
}
