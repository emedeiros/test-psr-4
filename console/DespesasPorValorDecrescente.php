<?php

/**
 * Author: Evandro Ishy Medeiros
 * Description: Retorna, no terminal, as despesas ordernadas(somatória dos valores) de forma decrescente
 */

include(__DIR__."/../config/autoload.php");

$despesa = new Despesa();

$result = $despesa->despesasOrdenadasDecrescente();

foreach($result as $desp)
{
    echo "Despesa: " . $desp['nome'] . " -Valor- " . $desp['valor'] . "\n\n";
}
