<?php

/**
 * Author: Evandro Ishy Medeiros
 * Description: Script para geração de banco de dados
 */

/*Autoload*/
include(__DIR__.'/../config/autoload.php');

/*Classe existente na pasta database, cria o banco de dados e a estrutura.*/
$database = new GenerateDb();

/*Se o banco de dados já existe encerra nesse ponto*/
if(!$database->createDb())
{
    echo $database->getError();
    exit();
}

/*Se ocorrer algum erro durante a criação das tabelas é encerrado*/
if(!$database->createTables())
{
    echo $database->getErrors();
    exit();
}

echo "Banco de dados criado com sucesso.\n";