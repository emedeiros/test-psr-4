<?php

/**
 * Futura implementação de uma classe para página web.
 */

Class Controller 
{
    private $data;

    public function __construct()
    {
        $this->data['titulo'] = 'Teste Meliuz - Gastos da Assembleia legislativa';
        $this->data['tabelas'] = [];
        $this->data['message'] = "";
        $this->data['page_h1'] = "";
        $this->data['class_tr'] = "";
    }

    public function categorias()
    {
        $categorias = new Despesa();
        $this->data['titulo'] .= " - Inicio";
        $this->data['page_h1'] = "Categorias";
        $this->data['class_tr'] = "click";
        $this->data['colName1'] = "Categoria";
        $this->data['colName2'] = "Total de verba utilizada(R$)";

        $this->data['tabela'] = $categorias->despesasOrdenadasDecrescente();

        return $this->data;
    }

    public function deputados($id=0)
    {
        $objDespesa = new Despesa();
        $objDeputado = new Deputado();

        $nome = $objDespesa->get($id);

        if(empty($nome))
        {
            $this->data['message'] = "Categoria n&atilde;o encontrada. Tente novamente";
            return $this->categorias();
        }

        $this->data['titulo'] .= " - Categoria";
        $this->data['page_h1'] = "Categoria - " . $nome[0]['nome'];
        $this->data['class_tr'] = "";
        $this->data['colName1'] = "Deputados";
        $this->data['colName2'] = "Verba utilizada na categoria(R$)";
        $this->data['tabela'] = $objDeputado->porCategoria($id);
        return $this->data;
    }
}