<?php

/**
 * Author: Evandro Ishy Medeiros
 * Description: Helper com duas funções genéricas.
 */

/**
 * Função para conexão no webservice
 */
function get($url)
{
    $channel = curl_init($url);
    curl_setopt($channel, CURLOPT_RETURNTRANSFER, True);
    curl_setopt($channel, CURLOPT_TIMEOUT, 30);
    $retorno = curl_exec($channel);
    return !$retorno ? "[]" : $retorno;
}

/**
 * Função para validação de json
 */
function isJson($data)
{
    $retorno = json_decode($data);
    if(json_last_error()== JSON_ERROR_NONE)
        return True;
    return False;
}
