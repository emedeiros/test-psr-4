<?php

/**
 * Author: Evandro Ishy Medeiros
 * Description: Model para inserção e recuperação de dados dos deputados.
 * Armazena apenas os dados necessários para o teste.
 */

Class Deputado Extends AbstractGeneric
{

    protected $path;
    protected $name;
    protected $error;

    public function __construct($dbPath=DB_PATH, $dbName=DB_NAME)
    {
        $this->path = $dbPath;
        $this->name = $dbName;
        $this->error = "";
    }

    /**
     * Método para inserção do id do deputado e nome
     */
    public function insert($deputado)
    {
        if($deputado->nome==="" || !is_int($deputado->id) || $deputado->id < 1)
        {
            $this->error = "Dado Inválido\n";
            return False;
        }
        $db = $this->newSqliteConnection();
        $db->exec("INSERT INTO Deputados(id, nome) VALUES(".$deputado->id.",'".str_replace("'", "''", $deputado->nome)."')");
        $db->close();
        
        return True;
    }

    /**
     * Método para busca de um OU todos os deputados no banco com possibilidade 
     * de buscar todos os campos ou apenas o campo necessário
     */
    public function get($elements=['id', 'nome'], $id = 0)
    {
        $db = $this->newSqliteConnection();

        if($id === 0)
            $rs = $db->prepare("SELECT ".implode(",", $elements)." FROM deputados");
        else
        {
            $rs = $db->prepare("SELECT ".implode(",", $elements)." FROM deputados WHERE id = :id");
            $rs->bindValue(":id", $id);
        }

        $result = $rs->execute();
        $retorno = $this->getArray($result);  
        $db->close();
        
        return $retorno;
    }

    /**
     * Método que retorna os deputados que mais gastaram no período de 2014
     */
    public function porCategoria($id=0, $limit = LIMIT_SEARCH)
    {
        $db = $this->newSqliteConnection();

        $rs = $db->prepare("SELECT deputados.id id, deputados.nome nome, indenizacoes.valor valor 
                            FROM Indenizacoes 
                               INNER JOIN Deputados ON deputados.id = indenizacoes.deputado_id
                            WHERE indenizacoes.despesa_id = :id
                            ORDER BY valor DESC
                            LIMIT " . $limit);

        $rs->bindValue(":id", $id);

        $resultado = $rs->execute();

        $retorno = $this->getArray($resultado); 
        $db->close();
        return $retorno;
    }

}
