<?php

/**
 * Author: Evandro Ishy Medeiros
 * Description: Model para inserção e recuperação de dados de indenização.
 * Armazena apenas os dados necessários para o teste.
 */

Class Indenizacao Extends AbstractGeneric
{
    protected $path;
    protected $name;
    protected $error;

    public $message;

    public function __construct($dbPath=DB_PATH, $dbName=DB_NAME)
    {
        $this->path = $dbPath;
        $this->name = $dbName;
        $this->error = "";
        $this->message = "";
    }

    /**
     * Este método, apesar de ter nome de insert, o mesmo realiza update quando necessário.
     * Como não foi pedido data ou períodos de quando ocorreram e sim apenas o total por ano e
     * quais foram os maiores gastos este método acaba verificado se o usuário já possui alguma
     * despesa naquele id e soma a nova despesa com a anterior.
     */
    public function insert($indenizacao)
    {
        $db = $this->newSqliteConnection();
    
        if(!is_int($indenizacao->despesa_id) || $indenizacao->despesa_id < 1 || !is_int($indenizacao->deputado_id) || $indenizacao->deputado_id < 1)
        {
            $this->error = "Dado inválido\n";
            return False;
        }

        if(empty($this->get($indenizacao)))
        {
            $db->exec("INSERT INTO Indenizacoes(deputado_id, despesa_id, valor) VALUES (".$indenizacao->deputado_id."," . $indenizacao->despesa_id . ",".str_replace("'","''", $indenizacao->valor).")");            
        }
        else
        {
            $db->exec("UPDATE Indenizacoes SET valor=valor+".$indenizacao->valor." WHERE deputado_id = ".$indenizacao->deputado_id." AND despesa_id = ". $indenizacao->despesa_id);
        }

        $this->message = "Atualizado a despesa de um deputado\n";
        $db->close();

        return True;
    }

    /**
     * Método que verifica se o deputado já tem alguma despesa com o id passado no banco.
     * Caso já possua o mesmo irá apenas atualizar o valor da despesa.
     */
    public function get($data)
    {
        $db = $this->newSqliteConnection();

        $rs = $db->prepare("SELECT valor FROM Indenizacoes WHERE deputado_id = :deputado_id AND despesa_id = :despesa_id");
        $rs->bindValue(":deputado_id", $data->deputado_id);
        $rs->bindValue(":despesa_id", $data->despesa_id);

        $result = $rs->execute();
        $retorno = $this->getArray($result);  
        $db->close();
        
        return $retorno;
    }
    
}
