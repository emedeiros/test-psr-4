<?php

/**
 * Author: Evandro Ishy Medeiros
 * Description: Model para inserção e recuperação de dados de despesas.
 * Armazena apenas os dados necessários para o teste.
 */

Class Despesa Extends AbstractGeneric
{
    protected $path;
    protected $name;
    protected $error;

    public $message;

    public function __construct($dbPath=DB_PATH, $dbName=DB_NAME)
    {
        $this->path = $dbPath;
        $this->name = $dbName;
        $this->error = "";
        $this->message = "";
    }


    /**
     * Método para inserção de uma nova despesa
     */
    public function insert($despesa)
    {
        $db = $this->newSqliteConnection();

        if(!is_int($despesa->despesa_id) || $despesa->despesa_id < 1 || strlen($despesa->despesa_nome)===0)
        {
            $this->error = "Dado inválido\n";
            return  False;
        }
        /*
         Caso a despesa não exista é inserida e dado um alerta de inserção
         (Feito o alerta para não dar a impressão de programa travado)
        */
        if(empty($this->get($despesa->despesa_id)))
        {
            $this->message = "Nova Despesa ".$despesa->despesa_nome." inserida\n";
            $db->exec("INSERT INTO despesas(id, nome) VALUES(".$despesa->despesa_id.",'".str_replace("'","''",$despesa->despesa_nome)."')");
        }
        else /*Caso a despesa já exista é dado um alerta que a mesma já foi inserida*/
        {
            $this->message = "Despesa ".$despesa->despesa_nome." já existente\n";
        }
    
        $db->close();
        return True;
    }

    /**
     * Método utilizado pelo insert para verificação de existência da despesa
     * Deixado como método público para futura utilização, caso necessário, na criação de uma página web.
     */
    public function get($id = 0)
    {
        $db = $this->newSqliteConnection();

        $rs = $db->prepare("SELECT id, nome FROM despesas WHERE id = :id");
        $rs->bindValue(":id", $id);

        $result = $rs->execute();
        $retorno = $this->getArray($result);  
        $db->close();
        
        return $retorno;
    }

    /**
     * Método para retorno das despesas ordenada de forma decrescente
     */
    public function despesasOrdenadasDecrescente()
    {
        $db = $this->newSqliteConnection();

        $rs = $db->prepare("SELECT despesas.id, despesas.nome, SUM(indenizacoes.valor) valor 
                      FROM despesas
                        INNER JOIN indenizacoes ON despesas.id = indenizacoes.despesa_id
                      GROUP BY despesas.nome
                      ORDER BY SUM(indenizacoes.valor) DESC");

        $result = $rs->execute();
        $retorno = $this->getArray($result);  
        $db->close();
        
        return $retorno;
    }

}
